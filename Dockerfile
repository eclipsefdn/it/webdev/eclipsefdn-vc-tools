#
# To build the generic node.js container, run the following command: `docker build -f Dockerfile -t local/vc-sync:latest .`
#
# To start the generic container, use the following as a template, setting the source of the secrets volume as appropriate 
# for the current run: docker run -v <dir to applicable secrets>:/run/secrets --rm --entrypoint="" --memory=2g -it local/github-sync:latest bash 
#
# To start the generic container for GitHub runs, use the following command once the container starts, changing the params as necessary:
# npm start -- --dryrun=true --devMode=true --verbose=true
# 
# To start the generic container for GitLab runs, use the following command once the container starts, changing the params as necessary:
# npm run lab-sync -- --dryrun=true --devMode=true --verbose=true
#
# To start the generic container for auto-backup runs, use the following command once the container starts, changing the params as necessary:
# npm run import-backup -- --dryrun=true --secretLocation=/run/secrets  --mail=false
#
FROM node:lts-hydrogen
WORKDIR /app

## Copy over required script files
COPY src src/

## Set up the cache for the run outside of the root-owned namespace
RUN mkdir .npm \
&& npm config set cache .npm --global

## Copy NPM configs and install dependencies
COPY package*.json ./
COPY tsconfig.json ./
RUN npm ci --ignore-scripts
