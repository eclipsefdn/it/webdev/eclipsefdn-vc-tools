const HOUR_IN_SECONDS = 3600;
const EXIT_ERROR_STATE = 1;

const { getLogger } = require('./logger.js');
const axios = require('axios');
const parse = require('parse-link-header');
const { ClientCredentials } = require('simple-oauth2');

module.exports = class EclipseAPI {
  #config;
  #client;
  #accessToken;
  #verbose = false;
  set verbose(val) {
    if (typeof val === 'boolean') {
      this.#verbose = val;
      // update logger as well as it is dependent
      this.#logger = getLogger(this.#verbose ? 'debug' : 'info', 'EclipseAPI');
    }
  }
  get verbose() {
    return this.#verbose;
  }
  #testMode = false;
  get testMode() {
    return this.#testMode;
  }
  set testMode(testMode) {
    if (typeof testMode === 'boolean') {
      this.#testMode = testMode;
    }
  }
  #logger;

  constructor(config = {}) {
    this.#config = config;
    // if we have oauth config, intialize access token
    if (this.#config.oauth !== undefined) {
      const oauth = {
        client: {
          id: this.#config.oauth.client_id,
          secret: this.#config.oauth.client_secret,
        },
        auth: {
          tokenHost: this.#config.oauth.endpoint,
          tokenPath: '/oauth2/token',
          authorizePath: '/oauth2/authorize',
        },
      };
      this.#client = new ClientCredentials(oauth);
    }
    this.#logger = getLogger('info', 'EclipseAPI');
  }

  async eclipseAPI(queryStringParams = '', paginate = true) {
    if (this.#verbose) {
      this.#logger.debug(`EclipseAPI:eclipseAPI(queryStringParams = ${queryStringParams}, paginate = ${paginate})`);
    }
    // if test mode is enabled, return data that doesn't impact production
    if (this.#testMode) {
      this.#logger.debug('Test mode active, returning sample projects.');
      return [{
        project_id: 'spider.pig',
        short_project_id: 'spider.pig',
        name: 'Spider pig does what a spider pig does',
        summary: 'Can he fly? No, hes a pig. Look out, here comes the spider pig',
        logo: '',
        tags: ['simpsons', 'doh', 'spider pig'],
        github_repos: [{
          url: 'https://github.com/eclipsefdn-webdev/spider-pig',
        }],
        github: {
          org: 'eclipsefdn-webdev',
          ignored_repos: [],
        },
        contributors: [],
        committers: [{
          username: 'malowe',
          url: 'https://api.eclipse.org/account/profile/malowe',
        }, {
          username: 'epoirier',
          url: 'https://api.eclipse.org/account/profile/epoirier',
        }],
        project_leads: [{
          username: 'malowe',
          url: 'https://api.eclipse.org/account/profile/malowe',
        }, {
          username: 'cguindon',
          url: 'https://api.eclipse.org/account/profile/cguindon',
        }],
        working_groups: [{
          name: 'Cloud Development Tools',
          id: 'cloud-development-tools',
        }],
        spec_project_working_group: [],
        state: 'Regular',
      }];
    }

    // Start 'hasMore' with paginate value to remove the need to check it later
    let hasMore = paginate;
    let result = [];
    let data = [];
    // add timestamp to url to avoid browser caching
    let url = 'https://projects.eclipse.org/api/projects' + queryStringParams;
    // loop through all available users, and add them to a list to be returned
    do {
      this.#logger.silly('Loading next page...');
      // get the current page of results, incrementing page count after call
      result = await axios.get(url).then(r => {
        // return the data to the user
        let links = parse(r.headers.link);
        if (links.self.url === links.last.url) {
          hasMore = false;
        } else {
          url = links.next.url;
        }
        return r.data;
      }).catch(err => this.#logger.error(`Error while retrieving results from Eclipse Projects API (${url}): ${err}`));

      // collect the results
      if (result != null && result.length > 0) {
        for (const resultItem of result) {
          data.push(resultItem);
        }
      }
    } while (hasMore);
    return data;
  }

  postprocessEclipseData(data, param) {
    if (this.#verbose) {
      this.#logger.debug(`EclipseAPI:postprocessEclipseData(data = (ommitted, ${data.length} projects), param = ${param})`);
    }
    for (let key in data) {
      let project = data[key];
      // add post processing fields
      project.pp_repos = [];
      project.pp_orgs = [];
      let repos = project[param];
      // if the length of repos is 0, then we have nothing to process. If the org isn't set, then remove it from the set
      if (repos.length === 0 && project.github.org !== '') {
        // this check will need to be improved when GH sync is migrated to TS and shares code, but this works for now
        continue;
      } else if (repos.length === 0) {
        delete data[key];
        continue;
      }
      // update the projects for post processing
      repos.forEach(e => this.handleProjectRepo(e, project));
      // set back to ensure properly set
      data[key] = project;
    }
    return data.filter(p => p.state !== undefined && p.state.toLowerCase() !== 'archived');
  }

  /**
   * Used for post processing of project repositories to add custom variables to add some contextual data to projects.
   *
   * @param {*} repo one of the repos associated with the project
   * @param {*} project the project that is being currently processed
   */
  handleProjectRepo(repo, project) {
    let repoUrl = repo.url;

    this.#logger.debug(`Checking repo URL: ${repoUrl}`);
    // strip the repo url to get the org + repo
    let match = /.*\/([^/]+)\/([^/]+)\/?$/.exec(repoUrl);
    // check to make sure we got a match
    if (match === null) {
      this.#logger.warn(`No match for URL ${repoUrl}`);
      return;
    }

    // get the org + repo from the repo URL
    let org = match[1];
    let repoName = match[2];
    // set the computed data back to the objects
    repo.org = org;
    repo.repo = repoName;
    if (project.pp_orgs.indexOf(org) === -1) {
      this.#logger.verbose(`Found new match, registered org=${org}`);
      project.pp_orgs.push(org);
    }
    if (project.pp_repos.indexOf(repoName) === -1) {
      this.#logger.verbose(`Found match, registered repo=${repoName}`);
      project.pp_repos.push(repoName);
    }
  }

  async eclipseUser(username) {
    if (this.#verbose) {
      this.#logger.debug(`EclipseAPI:eclipseUser(username = ${username})`);
    }
    return await axios.get('https://api.eclipse.org/account/profile/' + username, {
      headers: {
        Authorization: `Bearer ${await this._getAccessToken()}`,
      },
    })
      .then(result => result.data)
      .catch(err => this.#logger.error(`${err}`));
  }

  async eclipseBots() {
    if (this.#verbose) {
      this.#logger.debug('EclipseAPI:eclipseBots()');
    }
    let botsRaw = await axios.get('https://api.eclipse.org/bots')
      .then(result => result.data)
      .catch(err => this.#logger.error(`${err}`));
    if (botsRaw === undefined || botsRaw.length <= 0) {
      this.#logger.error('Could not retrieve bots from API');
      process.exit(EXIT_ERROR_STATE);
    }
    return botsRaw;
  }

  processBots(botsRaw, site = 'github.com') {
    if (this.#verbose) {
      this.#logger.debug(`EclipseAPI:processBots(botsRaw = (ommitted, ${botsRaw.length} bots), site = ${site})`);
    }
    let rgx = new RegExp(`^${site}.*`);
    let botMap = {};
    for (const bot of botsRaw) {
      // get the list of bots for project if already created
      let projBots = botMap[bot['projectId']];
      if (projBots === undefined) {
        projBots = [];
      }
      // get usernames for site + sub resource bots
      let botKeys = Object.keys(bot);
      for (let idx in botKeys) {
        let key = botKeys[idx];
        // if there is a match (either direct or sub resource match) push the bot name to the list
        let match = key.match(rgx);
        if (match) {
          projBots.push(bot[key]['username']);
        }
      }
      // dont add empty arrays to output
      if (projBots.length === 0) {
        continue;
      }
      botMap[bot['projectId']] = projBots;
    }
    return botMap;
  }

  async _getAccessToken() {
    if (this.#accessToken === undefined || this.#accessToken.expired(this.#config.oauth.timeout || HOUR_IN_SECONDS)) {
      try {
        this.#accessToken = await this.#client.getToken({
          scope: this.#config.oauth.scope,
        });
      } catch (error) {
        this.#logger.error(error);
        process.exit(EXIT_ERROR_STATE);
      }
      return this.#accessToken.token.access_token;
    }
  }
};

