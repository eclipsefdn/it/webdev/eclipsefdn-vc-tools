
// set up yargs command line parsing
const argv = require('yargs')
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('d', {
    alias: 'dryrun',
    description: 'Runs script as dry run, not writing any changes to API',
    boolean: true,
  })
  .option('T', {
    alias: 'target',
    description: 'Target organization(s) to update',
  })
  .option('i', {
    alias: 'inverse',
    description: 'Inverse the target argument to exclude targeted groups',
    boolean: true,
  })
  .option('t', {
    alias: 'devMode',
    description: 'Runs script in dev mode, which returns API data that does not impact production organizations/teams. '
      + 'This does NOT affect the static team manager. If testing is wanted for that integration, verbose and dryrun mode are suggested.',
    boolean: true,
  })
  .option('V', {
    alias: 'verbose',
    description: 'Sets the script to run in verbose mode',
    boolean: true,
  })
  .option('D', {
    alias: 'deletionDryRun',
    description: 'Runs the script in a semi-dryrun state to prevent deletions of users',
    boolean: true,
  })
  .option('p', {
    alias: 'project',
    description: 'The project ID that should be targeted for this sync run',
  })
  .option('s', {
    alias: 'secretLocation',
    description: 'The location of the access-token file containing an API access token',
    default: '/run/secrets/'
  })
  .help('h')
  .alias('h', 'help')
  .version('0.1')
  .alias('v', 'version')
  .epilog('Copyright 2024 Eclipse Foundation inc.')
  .argv;
const { GithubSync } = require('./Sync');
run();

/**
 * runs the sync process using the CLI args as configuration items to pass to the sync runner.
 */
async function run() {
  console.log(`Arguments: ${JSON.stringify(argv)}`);
  await new GithubSync(argv).prepareSecret();
}
