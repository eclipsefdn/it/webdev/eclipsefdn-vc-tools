/** ***************************************************************
 Copyright (C) 2025 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

export interface StaticTeam {
  repos: string[];
  name: string;
  members: [
    {
      name: string;
      url: string;
      expiration: string;
    }
  ];
  permission: string;
  description: string;
  privacy: string;
  expiration: string;
  serviceTeamFor: string;
}
