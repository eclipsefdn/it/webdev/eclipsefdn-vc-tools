/** *****************************************************************************
 * Copyright (C) 2019 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

// custom wrappers
const Wrapper = require('./GitWrapper.js');
const { HttpWrapper } = require('./HttpWrapper.js');
const EclipseAPI = require('./EclipseAPI.js');

const DEFAULT_ORG_PERMISSIONS = {
  default_repository_permission: 'none',
  members_can_create_repositories: false,
  members_can_create_private_repositories: false,
  members_can_create_public_repositories: false,
  members_allowed_repository_creation_type: 'none',
};
const DENYLIST_ORGS_ORG_WIDE_PROJECT = ['eclipse'];
const DENYLIST_ORG_READ_UPDATES = ['openhwgroup'];
const DENYLIST_ORG_PERM_UPDATES = [];
const HTTP_NOT_FOUND_STATUS = 404;
const SECURITY_REPO_NAME = '.eclipsefdn';
const INTERNAL_TEAM_NAMES = ['eclipsefdn-security', 'eclipsefdn-releng'];
const NOT_FOUND_MESSAGE = 'User not found.';

const { getLogger } = require('./logger.js');
const { SecretReader, getBaseConfig } = require('./SecretReader.js');
const { StaticTeamManager, ServiceTypes } = require('./teams/StaticTeamManager.js');
const { UserPermissionsOverride } = require('./teams/UserPermissionsOverride.js');

const GithubSync = class {
  argv;
  logger;

  // create global placeholder for wrapper
  wrap;
  cHttp;
  eclipseApi;
  bots;
  stm;
  upo;

  constructor(args) {
    this.argv = args;
    this.logger = getLogger(this.argv.V ? 'debug' : 'info', 'main');
  }

  /**
   * Retrieves secret API token from system, and then starts the script via _init
   *
   * @returns
   */
  async prepareSecret() {
    // retrieve the secret API file root if set
    const settings = getBaseConfig();
    if (this.argv.s !== undefined) {
      settings.root = this.argv.s;
    }
    const reader = new SecretReader(settings);
    // get the secret and start the script if set
    const secret = reader.readSecret('api-token');
    const privateKey = reader.readSecret('private-key');
    const appId = reader.readSecret('app-id');
    if (privateKey !== null && appId !== null) {
      this._init(secret.trim(), appId.trim(), privateKey.trim());
    }
  }

  /**
   * Initializes and starts the sync process for the script. This will initialize all of the internal service wrappers, pre-cache assets to
   * reduce calling to various APIs, and set up the environment before running then cleaning up the state afterwards.
   *
   * @param secret the GitHub access token to use for reading and writing data to the GH API.
   */
  async _init(secret, appId, privateKey) {
    if (secret === undefined || secret === '') {
      this.logger.error('Could not fetch API secret, exiting');
      return;
    }
    this.wrap = new Wrapper(secret || '', appId, privateKey);
    await this.wrap.init();
    if (!await this.wrap.checkAccess()) {
      return;
    }
    this.wrap.setDryRun(this.argv.d);
    this.wrap.setVerbose(this.argv.V);
    this.logger.info(`Running in dryrun? ${this.argv.d}`);
    this.logger.info(`Running in test mode? ${this.argv.t}`);

    this.cHttp = new HttpWrapper();
    this.stm = new StaticTeamManager();
    this.stm.verbose = this.argv.V;

    // create the user permissions object
    try {
      this.upo = new UserPermissionsOverride(`${this.argv.s}/permissions.json`);
      this.upo.verbose = this.argv.V;
    } catch (e) {
      this.logger.error(`Unable to load permission overrides, cannot continue processing: ${e}`);
      process.exit(1);
    }

    this.eclipseApi = new EclipseAPI();
    this.eclipseApi.verbose = this.argv.V;
    this.eclipseApi.testMode = this.argv.t;
    // get raw project data and post process to add additional context
    let data = this.processProjects(await this.eclipseApi.eclipseAPI('?github_only=1'));
    data = this.eclipseApi.postprocessEclipseData(data, 'github_repos');

    this.logger.info(`Finished preloading ${data.length} projects`);
    // get this.bots for raw project processing
    const rawBots = await this.eclipseApi.eclipseBots();
    this.bots = this.eclipseApi.processBots(rawBots);
    this.logger.info(`Found ${Object.keys(this.bots).length} registered bots`);

    // start the sync operation.
    await this.runSync(data);

    // close the this.wrappers, persisting required cache info
    this.cHttp.close();
  }

  /**
   * Using base projects data, retrieve any overrides relevant for the project, apply the updates, and return the modified project data.
   *
   * @param projectsData the Eclipse Foundation projects that should be processed.
   * @return the processed and updated project information, including overrides
   */
  processProjects(projectsData) {
    return projectsData.map(project => {
      const pid = project['project_id'];

      // for the project, look up overridden users and add the users to the lists accordingly
      const overrides = this.upo.getUsersForProject(pid);
      if (this.argv.V && overrides.length > 0) {
        this.logger.info(`Found ${overrides.length} for project ${pid}, applying to user lists`);
      }
      overrides.forEach(override => {
        // set up the user object that will be injected for this override
        const userObj = {
          username: override.name,
          url: `https://api.eclipse.org/account/profile/${override.name}`
        };
        if ('triage'.localeCompare(override.permission, undefined, { sensitivity: 'base' }) === 0) {
          this.logger.info(`Adding user ${override.name} to project '${pid}' as a contributor as override`);
          project.contributors.push(userObj);
        } else if ('write'.localeCompare(override.permission, undefined, { sensitivity: 'base' }) === 0) {
          this.logger.info(`Adding user ${override.name} to project '${pid}' as a committer as override`);
          project.committers.push(userObj);
        } else if ('maintain'.localeCompare(override.permission, undefined, { sensitivity: 'base' }) === 0
          || 'admin'.localeCompare(override.permission, undefined, { sensitivity: 'base' }) === 0) {
          this.logger.info(`Adding user ${override.name} to project '${pid}' as a project lead as override`);
          project.project_leads.push(userObj);
        } else {
          this.logger.info(`Unsupported override permission of ${override.permission} ignored for user ${override.name} in project ${pid}`);
        }
      });

      // for each of the user lists, check that the users aren't blocked from access
      project.committers = project.committers.filter(committer =>
        this.upo.checkUserForOverride(committer.username, pid) !== 'blocked');
      project.project_leads = project.project_leads.filter(pl =>
        this.upo.checkUserForOverride(pl.username, pid) !== 'blocked');
      project.contributors = project.contributors.filter(contributor =>
        this.upo.checkUserForOverride(contributor.username, pid) !== 'blocked');

      return project;
    });
  }

  /**
   * Main runner for the sync operations. Accepts the projects to be synced and performs that various operations to manage access
   * permissions for the project according to PMI data and some internal static mappings.
   *
   * - create and update teams for contributors, committers, project leads, and security team. This includes the following operations:
   *   - Actual creation of the teams
   *   - Adding/removing users according to PMI and internal temporary overrides
   *   - maintaining created team visibility + descriptions to be as open as possible
   *   - associate teams to the proper repos to give access according to PMI parameters
   * - remove external contributors on organizations
   * - sync non-PMI internal static teams to various repositories for staff access
   *
   * @param {*} projects list of projects to sync
   */
  async runSync(projects) {
    const start = new Date();
    // used to track which orgs have been processed for removing outside collabs
    const uniqueOrgs = [];
    for (const key in projects) {
      const project = projects[key];

      // check that the project matches the project target if set
      const projectID = project.project_id;
      if (this.argv.p !== undefined && projectID !== this.argv.p) {
        this.logger.info(`Project target set ('${this.argv.p}'). Skipping non-matching project ${projectID}`);
        continue;
      }
      this.logger.info(`Project ID: ${projectID}`);

      // Retrieve both distributed and organization wide project repos
      let repos = await this.handleDistributedProjects(project);
      const orgWideRepos = await this.handleOrgWideProject(project);
      if (orgWideRepos !== null) {
        repos = repos.concat(orgWideRepos);
      }
      // deduplicate repos, find index will eject any entries that don't match the first index of a unique ID
      repos = this.filterDuplicateRepositories(repos);

      // updates the orgs ahead of time by grabbing unique
      const projectOrgs = repos
        .map(r => r.org)
        .filter((orgName, index, originalArray) => originalArray.indexOf(orgName) === index);
      // if empty array, then no repos could be found but org is valid and should be included
      if (orgWideRepos !== null && orgWideRepos.length === 0) {
        projectOrgs.push(project.github.org);
      }
      // process each of the orgs, adding teams and cleaning outside collaborators
      for (let orgIdx in projectOrgs) {
        const org = projectOrgs[orgIdx];
        this.logger.info(`Generating teams for ${org} for project ${projectID}`);
        // process the org to add teams
        await this.processProjectsOrg(org, project);

        // remove external contributors as well
        if (!uniqueOrgs.includes(org)) {
          this.logger.info(`Removing outside collaborators for ${org}`);
          await this.removeOrgExternalContributors(projects, org);
          uniqueOrgs.push(org);
        }
      }

      // process the repositories for this project using the teams generated earlier
      await this.processRepositories(repos, project);
    }

    this.logger.info('Beginning processing of static teams');
    // retrieve the static teams for GitHub
    const teams = this.stm.processTeams(ServiceTypes.GITHUB);
    if (this.argv.V) {
      this.logger.info(`Number of custom teams discovered: ${teams.length}`);
    }
    for (const tIdx in teams) {
      const team = teams[tIdx];
      // process each team individually
      await this.processStaticTeam(team, uniqueOrgs);
    }

    // log how long it took to do this stuff
    const end = new Date();
    this.logger.info(`Start: ${start}, end: ${end}, calls: ${this.wrap.getCallCount()}`);
  }

  /**
   * Retrieves and formats repos from organization into simplified format for use in repository processing in later
   * stages. These orgs are unfiltered and should represent all repos within an org.
   */
  async handleOrgWideProject(project) {
    // check that this code should be used in this project
    const projectOrg = project.github.org;
    if (projectOrg === undefined || projectOrg.trim() === ''
      || DENYLIST_ORGS_ORG_WIDE_PROJECT.includes(projectOrg.trim().toLowerCase())) {
      this.logger.info(`Project ${project.project_id} does not have a valid Github project org set, not using org wide logic`);
      return null;
    }

    // check if the designated org is available for current run
    if (this.argv.T && projectOrg.trim().localeCompare(this.argv.T) !== 0) {
      this.logger.info(`Project ${project.project_id} is outside of target org ${this.argv.T} and will be skipped.`);
      return null;
    }

    // get the repos from Github for current organization and convert into simplified format
    const orgRepos = await this.wrap.getReposForOrg(projectOrg.trim().toLowerCase());
    if (orgRepos === undefined || orgRepos.length === 0) {
      this.logger.info(`Error while retrieving repos associated with org ${projectOrg.trim().toLowerCase()}`);
      return [];
    }

    // if we can find repos, filter out the ignored repos
    this.logger.info(`Found ${orgRepos.length} potential repos using org-wide retrieval logic`);
    return orgRepos.filter(r => project.github.ignored_repos.indexOf(r.name) === -1
      && r.name.localeCompare(SECURITY_REPO_NAME) !== 0).map(r => {
      return {
        org: projectOrg,
        repo: r.name,
      };
    });
  }

  /**
   * Retrieves and filters repos based on the organization target in the arguments without further mutating the EF provided project data.
   */
  async handleDistributedProjects(project) {
    let repos = project.github_repos;
    // if org target set, then filter the projects
    if (this.argv.T) {
      repos = repos.filter(v => this.argv.i ? v.org !== this.argv.T : v.org === this.argv.T);
      this.logger.info(`Filtered ${project.github_repos.length} repos to ${repos.length} repos after applying target ${this.argv.T} `
        + `(inverse? ${this.argv.i})`);
    }
    return repos;
  }

  /**
   * Given a set of repositories and the associated project, the repository will be cleaned of external access permissions and then have
   * access granted through the teams managed by this script.
   *
   * @param {*} repos the GitHub repository object references that we will be syncing for the given project
   * @param {*} project the EF project that corresponds to the repositories being processed.
   */
  async processRepositories(repos, project) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:processRepositories(repos = ${JSON.stringify(repos)}, project = ${project.project_id})`);
    }
    for (let idx in repos) {
      const repo = repos[idx];
      const org = repo.org;
      const repoName = repo.repo;
      this.logger.info(`Starting sync for org=${org};repo=${repoName}`);

      // process contributors for the team
      const successfullyUpdatedExt = await this.removeRepoExternalContributors(project, org, repoName);
      if (this.argv.V === true) {
        this.logger.verbose(`Updated external contributors: ${successfullyUpdatedExt}`);
      }
      if (!this.argv.d) {
        try {
          // Ensure that the teams refer to the repo
          const updatedCommit = await this.wrap.addRepoToTeam(org, `${project.project_id}-committers`, repoName, 'push');
          const updatedContrib = await this.wrap.addRepoToTeam(org, `${project.project_id}-contributors`, repoName, 'triage');
          const updatedPL = await this.wrap.addRepoToTeam(org, `${project.project_id}-project-leads`, repoName, 'maintain', false);
          if (this.argv.V === true) {
            this.logger.verbose(`Attempted update commit team: ${updatedCommit === undefined}`);
            this.logger.verbose(`Attempted update contrib team: ${updatedContrib === undefined}`);
            this.logger.verbose(`Attempted update pl team: ${updatedPL === undefined}`);
          }
        } catch (e) {
          this.logger.error(`Error while updating ${project.project_id}. \n${e}`);
        }
      } else {
        this.logger.debug(`Dry run set, not adding repo '${repoName}' for org: ${org}`);
      }
    }
  }

  /**
   * Accepts a team and the list of unique organizations affected by this sync run. The unique orgs will be used to add
   * service type team to orgs when present.
   *
   * @param {*} team the static team definition to sync
   * @param {*} uniqueOrgs list of unique organizations that have been impacted by this sync run.
   */
  async processStaticTeam(team, uniqueOrgs) {
    const orgs = [];
    this.logger.info(`Processing static team ${team.name}`);
    // check if we are processing a service team or a standard static team
    if (team.serviceTeamFor !== '' && uniqueOrgs !== undefined && uniqueOrgs.length > 0) {
      this.logger.info(`Processing team '${team.name}' for ${uniqueOrgs.length} organizations`);
      // iterate over the unique orgs and add the static team to them
      for (const oIdx in uniqueOrgs) {
        // get the current org
        const org = uniqueOrgs[oIdx];
        // check expiration and then add the team to the org
        if ((await this.checkStaticTeamExpiration(team, org)) === true) {
          this.logger.info(`Static team '${team.name}' has expired, has been removed from the organization ${org}, and will not be active`);
          continue;
        }
        this.logger.info(`Generating static team '${team.name}' for organization ${org}`);
        await this.processStaticTeamForOrganization(org, team);

        // special logic for private foundation static teams
        if (INTERNAL_TEAM_NAMES.includes(team.name)) {
          this.logger.info(`Attempting to add ${team.name} team to ${SECURITY_REPO_NAME} for org ${org}`);
          await this.wrap.addRepoToTeam(org, team.name, SECURITY_REPO_NAME, 'push');
        }
      }
    } else {
      for (const repoUrl of team.repos) {
        await this.processStaticTeamRepos(team, repoUrl, orgs);
      }
    }
  }

  /**
   * Handles processing surrounding the indiviudal repo URLs assocaited with static teams. Checks for expiration, handles team creation in
   * case of cross-org static teams, and adding the repo in question to the static team.
   *
   * @param {*} team the static team definition being processed
   * @param {*} repoURL URL of the repo being processed in context of the static team
   * @param {*} trackedOrgs list of organizations where the static teams have already been created.
   */
  async processStaticTeamRepos(team, repoURL, trackedOrgs) {
    const match = /\/([^/]+)\/([^/]+)\/?$/.exec(repoURL);
    // check to make sure we got a match
    if (match == null) {
      this.logger.warn(`Cannot match repo and org from repo URL ${repoURL}, skipping`);
      return;
    }

    // get the org + repo from the repo URL
    const org = match[1];
    const repoName = match[2];
    if (this.argv.V) {
      this.logger.info(`Processing static team ${team.name} for repo ${repoName} in org ${org}`);
    }
    // check expiration and then add the team to the org
    if ((await this.checkStaticTeamExpiration(team, org)) === true) {
      this.logger.info(`Static team '${team.name}' has expired, has been removed from the organization ${org}, and will not be active`);
      return;
    }
    if (!trackedOrgs.includes(org)) {
      this.logger.info(`Generating teams for ${org}/${repoName}`);
      await this.processStaticTeamForOrganization(org, team);
      trackedOrgs.push(org);
    }
    if (!this.argv.d) {
      try {
        // update the team to have access to the repository
        await this.wrap.addRepoToTeam(org, this.wrap.sanitizeTeamName(team.name), repoName, team.permission);
      } catch (e) {
        this.logger.warn(`Error while updating ${this.wrap.sanitizeTeamName(team.name)}. \n${e}`);
      }
    } else {
      this.logger.debug(`Dry run set, not adding repo '${repoName}' to team '${this.wrap.sanitizeTeamName(team.name)}' `
        + `for org: ${org}`);
    }
  }

  /**
   * Checks if a static team should be removed from the given organization using the expiration date.
   */
  async checkStaticTeamExpiration(team, org) {
    // check if team is expired and should be deleted/skipped
    if (team.expiration !== undefined) {
      const expirationDate = new Date(team.expiration);
      // check if the expiration value is valid and after now
      if (expirationDate.getTime() < Date.now()) {
        this.logger.info(`Team with name ${team.name} is expired, it will be removed if present`);
        await this.wrap.removeTeam(org, this.wrap.sanitizeTeamName(team.name));
        return true;
      }
    }
    return false;
  }

  /**
   * Using the GitHub organization and EF project, project teams for contributors, committers, project leads, and security are updated.
   * These updates include additional permisison updates such as granting access to the private security repo, currently named .eclipsefdn
   *
   * @param {*} org the GH organization name being updated.
   * @param {*} project the EF project defninition containing access permissions and project metadata.
   */
  async processProjectsOrg(org, project) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:processProjectsOrg(org = ${org}, project = ${project.project_id})`);
    }
    // prefetch teams to reduce redundant calls
    await Promise.allSettled([this.wrap.prefetchTeams(org), this.wrap.prefetchRepos(org)]);

    // create the teams for the current org + update perms
    if (!this.argv.d) {
      // split out org updates to allow optional operations on orgs
      let orgUpdates = [
        this.updateProjectTeam(org, project, 'contributors'),
        this.updateProjectTeam(org, project, 'committers'),
        this.updateProjectTeam(org, project, 'project_leads'),
        this.updateProjectTeam(org, project, 'security_team', `${project.project_id}-security`)];

      // check deny lists before adding the org permission updates
      if (DENYLIST_ORG_PERM_UPDATES.includes(org)) {
        this.logger.info(`Skipping updating permissions for org ${org} as it has been denylisted`);
      } else if (DENYLIST_ORG_READ_UPDATES.includes(org)) {
        this.logger.info(`Org ${org} will be updated with modified settings (read access default)`);
        orgUpdates.push(this.wrap.updateOrgPermissions(org, { ...DEFAULT_ORG_PERMISSIONS, default_repository_permission: 'read' }));
      } else {
        orgUpdates.push(this.wrap.updateOrgPermissions(org, DEFAULT_ORG_PERMISSIONS));
      }

      const done = await Promise.allSettled(orgUpdates);
      this.logger.debug(`Finished creating teams for ${project.project_id} in org '${org}' with `
        + `${done.filter(status => status.status !== 'fulfilled').length} errors.`);

      // add the committers team to the private foundation team if it exists
      const updatedPrivateFdn = await Promise.allSettled([
        this.wrap.addRepoToTeam(org, this.wrap.sanitizeTeamName(`${project.project_id}-project-leads`), SECURITY_REPO_NAME, 'triage'),
        this.wrap.addRepoToTeam(org, this.wrap.sanitizeTeamName(`${project.project_id}-committers`), SECURITY_REPO_NAME, 'triage'),
      ]);
      this.logger.verbose(
        `${updatedPrivateFdn.filter(status => status.status !== 'fulfilled').length} errors updating committer + PL team (Private FDN team)`
      );
    } else {
      this.logger.debug('Dry run set, not adding teams for org: ' + org);
    }
  }

  /**
   * Taking the name of the GitHub organization and a static team definition, syncs the requisite team with roles and access permissions
   * to the organization.
   *
   * @param {*} org the name of the GitHub organization to sync the static team to.
   * @param {*} team the static team definition.
   */
  async processStaticTeamForOrganization(org, team) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:processOrg(org = ${org}, team = ${team})`);
    }
    // prefetch teams to reduce redundant calls
    await this.wrap.prefetchTeams(org);
    await this.wrap.prefetchRepos(org);
    const teamName = this.wrap.sanitizeTeamName(team.name);
    // create the teams for the current org
    if (!this.argv.d) {
      await this.updateTeam(org, { teamName: teamName, privacy: team.privacy, description: team.description }, team.members, undefined);
    } else {
      this.logger.debug(`Dry run set, not adding team '${teamName}' for org: ${org}`);
      if (this.argv.V) {
        this.logger.silly(`Would have added the following users to team '${teamName}': \n${JSON.stringify(team.members)}`);
      }
    }
  }

  /**
   * Syncs a singular project team given the grouping name to the organization. This will manage the teams metadata, as well as the
   * membership of the team using the PMI team grouping list. This will also manage whether the team can be public by the presence
   * of Otterdog configurations in the organization.
   *
   * @param {*} org name of the GH organization
   * @param {*} project the EF project being used as the base for the team
   * @param {*} grouping the user grouping being synced
   * @param {*} overrideName optional, if set will override the auto-generated team name
   */
  async updateProjectTeam(org, project, grouping, overrideName) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:updateProjectTeam(org = ${org}, project = ${project.project_id}, grouping = ${grouping},`
        + `override = ${overrideName})`);
    }
    const projectID = project.project_id;
    const teamName = this.wrap.sanitizeTeamName(overrideName || `${projectID}-${grouping}`);
    await this.updateTeam(org, {
      teamName: teamName,
      privacy: (await this.checkIfTeamCanBePublic(org)) ? 'closed' : 'secret',
      description: `The ${grouping} access team for the ${project.name} (${project.project_id}) project: ${project.url}`
    }, this.getProjectMembersForGrouping(project, grouping), project);
  }

  /**
   * Updates or creates a team for a GH organization given a set of properties as well as the list of final members for the team that
   * should be invited if missing. Should there be any additional people that aren't expected for a team, they will be removed as long
   * as there are no server errors in confirming identity of users, which is done as a safety precaution.
   *
   * @param {*} org name of the targeted GitHub organization
   * @param {*} teamProperties metadata properties for the team being updated
   * @param {*} designatedMembers list of users that should be members on the team
   * @param {*} project the EF project model that backs the given team
   */
  async updateTeam(org, teamProperties, designatedMembers, project) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:updateTeam(org = ${org}, teamProperties = ${JSON.stringify(teamProperties)}, designatedMembers = `
        + `${JSON.stringify(designatedMembers)}, project=${project?.project_id})`);
    }
    const teamName = teamProperties.teamName;
    this.logger.info(`Syncing team '${teamName}' for organization ${org}`);
    const team = await this.wrap.addTeam(org, teamName);
    if (team === undefined) {
      this.logger.warn(`Unable to create or find team '${teamName}', will not continue with updating team`);
      return;
    }

    // set team to private
    await this.wrap.editTeam(org, teamName, { privacy: teamProperties.privacy || 'secret', description: teamProperties.description || '' });
    let members = await this.wrap.getTeamMembers(org, team);

    // for each of the members, attempt to invite them, and update the members to remove handled users
    let shouldRemoveUsers = true;
    this.logger.debug(`Inviting ${designatedMembers.length} users to '${org}/${teamName}'`);
    for (const designatedMember of designatedMembers) {
      try {
        let user = await this.inviteUserToTeam(designatedMember, org, teamName);
        if (user !== undefined && members !== undefined) {
          // remove just the user that matches the username
          members = members.filter(e => e.login.localeCompare(user.github_handle, undefined, { sensitivity: 'base' }));
        }
      } catch (err) {
        // if there is an error during processing, we should not remove users, but we should continue attempting to add new users
        shouldRemoveUsers = false;
      }
    }

    await this.removeMembers(members, project, teamName, org, shouldRemoveUsers);
  }

  /**
   * Invites the given user to the team within the given organization.
   *
   * @param {*} designatedMember user reference for the user to be added to the team
   * @param {*} org the GitHub organization that owns the targeted team
   * @param {*} teamName name of the GitHub team that the user should be added to
   */
  async inviteUserToTeam(designatedMember, org, teamName) {
    // check if member has an expiration value set
    if (designatedMember.expiration !== undefined) {
      const expirationDate = new Date(designatedMember.expiration);
      // check if the expiration value is valid and after now
      if (expirationDate.getTime() < Date.now()) {
        this.logger.debug(`User ${designatedMember.username} has expired permissions and will not be added to the team.`);
        return;
      }
    }

    // get the user via cached HTTP
    const userRequest = await this.cHttp.getRaw(designatedMember.url);
    if (userRequest instanceof Error) {
      if (!this.checkIfUserIsMissing(userRequest.response)) {
        this.logger.error(`Error while fetching user data at ${designatedMember.url}, removal will be skipped for this team`
          + ' for this run');
        throw new Error();
      }
    } else {
      const user = userRequest.data;
      // check if github handle is null or empty
      if (!user.github_handle || user.github_handle.trim() === '') {
        this.logger.verbose(`User '${designatedMember.username}' has no associated GitHub username, skipping`);
        return;
      }

      // only invite when dryrun isn't set
      if (this.argv.d !== true) {
        // invite user to team
        await this.wrap.inviteUserToTeam(org, teamName, user.github_handle);
      } else {
        this.logger.debug(`Would have invited '${user.name}' to team ${teamName}, but in dry run mode`);
      }
      return user;
    }
  }

  /**
   * Checks and removes users from a project team, logging out any exceptions to the removal and handling bot account checks.
   *
   * @param {*} members members that exist on team that should be removed
   * @param {*} project the project associated with the team, used for bot account lookups
   * @param {string} teamName name of team that users would be removed from
   * @param {boolean} shouldRemoveUsers whether removal should be done for users on the teams
   */
  async removeMembers(members, project, teamName, org, shouldRemoveUsers = true) {
    this.logger.silly(`Leftover members: ${JSON.stringify(members)}`);
    // do some early checking to see if we should process the members
    if (members === undefined) {
      return;
    } else if (!shouldRemoveUsers) {
      this.logger.info('An error encountered while processing team members, users will not be removed from this team on this run');
      return;
    }
    // for each left over member, check if its a bot
    for (const member of members) {
      // bot check before deletion, skipping if user is bot
      if (!this.isUserBot(member.login, project)) {
        if (this.argv.D !== true) {
          this.logger.info(`Removing '${member.login}' from team '${teamName}'`);
          await this.wrap.removeUserFromTeam(org, teamName, member.login);
        } else {
          this.logger.debug(`Would have deleted '${member.login}', but in semi-dry run mode`);
        }
      } else {
        this.logger.verbose(`User '${member.login}' from team '${teamName}' identified as a bot, skipping`);
      }
    }
  }

  /**
   * Using the grouping name and the project, fetch the users that should be included for the passed grouping in the associated team.
   *
   * @param {*} project the EF project to source members from
   * @param {*} grouping the grouping within the project to fetch project members from, e.g. committers, contributors, project_leads,
   * security_team
   * @returns the list of associated users for the given group, or an empty list
   */
  getProjectMembersForGrouping(project, grouping) {
    if (grouping.localeCompare('security_team', undefined, { sensitivity: 'base' }) === 0) {
      // get the base list from the individual members
      const members = [...project.security_team.individual_members];
      // add members from other teams if flagged
      if (project.security_team.groups.include_committers) {
        members.push(...project.committers);
      }
      if (project.security_team.groups.include_project_leads) {
        members.push(...project.project_leads);
      }
      // deduplicate then return
      return members.filter((p1, i, a) => a.findIndex(p2 => p2.username.localeCompare(p1.username, undefined, { sensitivity: 'base' })
        === 0) === i);
    }
    return project[grouping] || [];
  }

  /**
   * Checks if the response from upstream indicates a missing user rather than another error. This will check the following
   * data points to confirm user is missing rather than server problems:
   *
   * - There was a response from the server
   * - Status is set to 404
   * - Response is an array that contains a single string indicating "User not found." OR Response is an object with a message string
   * indicating "User not found."
   *
   * @param response the Axios errors response property.
   * returns true if the user is missing, false if any other kind of error
   */
  checkIfUserIsMissing(response) {
    return response != null && response.status === HTTP_NOT_FOUND_STATUS && ((response.data instanceof Array
      && NOT_FOUND_MESSAGE.localeCompare(response.data[0], undefined, { sensitivity: 'base' }) === 0) || (response.data instanceof Object
        && NOT_FOUND_MESSAGE.localeCompare(response.data.message, undefined, { sensitivity: 'base' }) === 0));
  }

  /**
   * Checks whether a team can be set to public, using the presence of the Otterdog configuration repo in the same organization
   * as confirmation.
   *
   * @param {string} organization name of the Github organization to lookup for Otterdog presence
   * @returns true if Otterdog is enabled, false if the repo is not present or can't be confirmed.
   */
  async checkIfTeamCanBePublic(organization) {
    const orgRepos = await this.wrap.getReposForOrg(organization.trim().toLowerCase());
    if (orgRepos === undefined || orgRepos.length === 0) {
      this.logger.info(`Error while retrieving repos associated with org ${organization.trim().toLowerCase()},`
        + ' cannot confirm Otterdog presence');
      return false;
    }
    // check that one of the org repos is the .eclipsefdn repo that holds otterdog configurations
    return orgRepos.some(r => '.eclipsefdn'.localeCompare(r.name, undefined, { sensitivity: 'base' }) === 0);
  }

  async removeRepoExternalContributors(project, org, repo) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:removeRepoExternalContributors(project = ${project.project_id}, org = ${org}, repo = ${repo})`);
    }
    // get the collaborators
    const collaborators = await this.wrap.getRepoCollaborators(org, repo);
    if (collaborators === undefined) {
      this.logger.error(`Error while fetching collaborators for ${org}/${repo}`);
      return false;
    }
    // check if we have collaborators to process
    if (collaborators.length === 0) {
      return false;
    }

    const projBots = this.bots[project.project_id];
    for (const collabIdx in collaborators) {
      const uname = collaborators[collabIdx].login;
      // skip webmaster
      if (uname === 'eclipsewebmaster') {
        continue;
      }

      // get the this.bots for the current project
      if (projBots !== undefined && projBots.indexOf(uname) !== -1) {
        this.logger.verbose(`Keeping ${uname} as it was detected to be a bot for ${org}/${repo}`);
        continue;
      }

      // get the current users profile
      const url = `https://api.eclipse.org/github/profile/${uname}`;
      const userResult = await this.cHttp.getRaw(url);
      // If error received is 500, skip processing
      if (userResult instanceof Error) {
        if (!this.checkIfUserIsMissing(userResult.response)) {
          this.logger.error(`Error while fetching user data at ${url}, removal will be skipped for this team for this run`);
          continue;
        } else {
          this.logger.warn(`No user data could be retrieved for ${url}, but they will still be removed as a contributor on the repo`);
        }
      }

      // check user against list of project leads
      if (userResult.data != null) {
        const eclipseUserName = userResult.data.name;
        let isProjectLead = project['project_leads'].some(projectLead => projectLead.username === eclipseUserName);
        if (isProjectLead) {
          this.logger.verbose(`User '${eclipseUserName}' is a project lead for the current repository, not removing`);
          continue;
        }
      }
      // remove collaborator if we've gotten to this point and dryrun isn't set
      if (!this.argv.d) {
        this.logger.info(`Removing user '${uname}' from collaborators on ${org}/${repo}`);
        await this.wrap.removeUserAsCollaborator(org, repo, uname);
      } else {
        this.logger.verbose(`Dry run set, would have removing user '${uname}' from collaborators on ${org}/${repo}`);
      }
    }
    return true;
  }

  /**
   * Remove individuals added to the organization externally, which is an unsupported state for normal users in EF managed organizations.
   * The one exception to this rule is that bot users can be set in this role to help with deployments/access for builds.
   *
   * @param {*} projects list of Eclipse Foundation project used to determine bot status
   * @param {*} org targeted GitHub organization to remove external collaborators from
   */
  async removeOrgExternalContributors(projects, org) {
    if (this.argv.V === true) {
      this.logger.debug(`Sync:removeOrgExternalContributors(projects = (${projects.length} projects), org = ${org})`);
    }
    // get the collaborators
    const collaborators = await this.wrap.getOrgCollaborators(org);
    if (collaborators === undefined) {
      this.logger.error(`Error while fetching collaborators for ${org}`);
      return;
    }
    // check if we have collaborators to process
    if (collaborators.length === 0) {
      return;
    }
    // check each of the collaborators, removing them if they arent a bot for a
    // project in the org
    for (const collabIdx in collaborators) {
      const uname = collaborators[collabIdx].login;
      this.logger.verbose(`Checking collaborator '${uname}'...`);

      let isBot = false;
      const botKeys = Object.keys(this.bots);
      for (const botIdx in botKeys) {
        const botList = this.bots[botKeys[botIdx]];
        // check if the current user is in the current key-values list
        if (botList.indexOf(uname) !== -1) {
          this.logger.verbose(`Found user '${uname}' in bot list for project '${botKeys[botIdx]}', checking organizations`);
          // if we can determine that this user could be a bot, check that its
          // valid for current org
          for (const pIdx in projects) {
            const project = projects[pIdx];
            // check if our project ID is the ID associated with bot
            // and if the project has repositories within the given org
            if (project.project_id === botKeys[botIdx] && project.pp_orgs.indexOf(org) !== -1) {
              isBot = true;
              this.logger.verbose(`Discovered bot account for '${botKeys[botIdx]}' in org ${org}`);
              break;
            }
          }
        }
        // if we flagged the user as a bot, stop processing
        if (isBot) {
          break;
        }
      }
      // check if the user was flagged as a bot for the current org
      if (isBot) {
        this.logger.verbose(`Keeping '${uname}' as it was detected to be a bot for org '${org}'`);
        continue;
      }

      // remove collaborator if we've gotten to this point and dryrun isn't set
      if (!this.argv.d) {
        this.logger.info(`Removing user '${uname}' from collaborators on org '${org}'`);
        await this.wrap.removeUserAsOutsideCollaborator(org, uname);
      } else {
        this.logger.verbose(`Dry run set, would have removing user '${uname}' from collaborators on ${org}`);
      }
    }
  }

  /**
   * Checks the maintained bot mapping to see if the passed user is a bot for a project.
   *
   * @param {*} uname username of potential bot user
   * @param {*} project the EF project that corresponds to the current space in GitHub
   * @returns true if the user is a bot for a project, false otherwise
   */
  isUserBot(uname, project) {
    if (project !== undefined) {
      const botList = this.bots[project.project_id];
      // check if the current user is in the current key-values list for project
      if (botList && botList.indexOf(uname) !== -1) {
        this.logger.info(`Found user '${uname}' in bot list for project '${project.project_id}'`);
        return true;
      }
    }
    return false;
  }

  /**
   * Using the org and repo fields in each of the Eclipse project repository objects, check that each of the repos in the list are unique
   * to deduplicate bad inputs or mixed manual and org setting repos.
   *
   * @param {*} repos list of repository objects that are to be processed.
   * @returns list of deduplicated repositories
   */
  filterDuplicateRepositories(repos = []) {
    return repos.filter((p1, i, a) => a.findIndex(p2 => p2.repo.localeCompare(p1.repo, undefined, { sensitivity: 'base' }) === 0
      && p2.org.localeCompare(p1.org, undefined, { sensitivity: 'base' }) === 0) === i);
  }
};

// Export the class in the module for use in tests and callers
module.exports.GithubSync = GithubSync;
module.exports.NOT_FOUND_MESSAGE = NOT_FOUND_MESSAGE;
