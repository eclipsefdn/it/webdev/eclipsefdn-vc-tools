/** **************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

const { getLogger } = require('../logger.js');
const fs = require('fs');
// does input validation
const { ZodError } = require('zod');
// used to keep permissions consistent
const { ServiceTypes, PermissionsEnum } = require('./StaticTeamManager.js');
const { castJsonToOverrideFormat } = require('eclipsefdn-api-support');

const DEFAULT_FILE_ENCODING = 'utf-8';
const BLOCKED_PERMISSIONS_OUT = 'blocked';

class UserPermissionsOverride {
  // whether the app should be verbose in logging
  #verbose = false;
  set verbose(val) {
    if (typeof val === 'boolean') {
      this.#verbose = val;
      this.#logger = getLogger(this.#verbose ? 'debug' : 'info', 'UserPermissionsOverride');
    }
  }
  get verbose() {
    return this.#verbose;
  }

  // the logger for the permissions checking
  #logger;
  set logger(logger) {
    this.#logger = logger;
  }
  get logger() {
    return this.#logger;
  }

  // the user permissions data fetched from file.
  #data;

  /**
   * Creates an instance of the permissions override provider. Will load an external file and provide the ability to match users and
   * repo URLs to provide potential overrides for a given user.
   *
   * @param {string} location the filepath to the overrides file.
   */
  constructor(location = '', encoding = DEFAULT_FILE_ENCODING) {
    this.#logger = getLogger('info', 'UserPermissionsOverride');
    // read in the user permissions data and then check the format
    try {
      const data = fs.readFileSync(location, { encoding: encoding });
      if (data === undefined) {
        throw new Error('Incoming user permissions data cannot be empty');
      }

      // check the format using zod schema
      this.#data = castJsonToOverrideFormat(data.trim());
    } catch (e) {
      // cast and message with error
      let message;
      if (e.code === 'ENOENT') {
        message = `File at path ${location} does not exist`;
      } else if (e.code === 'EACCES') {
        message = `File at path ${location} cannot be read`;
      } else if (typeof e === ZodError) {
        message = 'The format of the user permissions source is incorrect and can\'t be loaded';
      } else if (typeof e === 'string') {
        message = 'An unknown error occurred while reading the user permissions override file';
      } else {
        message = 'An unknown error occurred while reading the user permissions override file';
      }
      // log the error and then throw, as we want to notify upstream that this is in an error state
      this.#logger.error(e);
      this.#logger.error(message);
      throw new Error(message);
    }
  }

  /**
   * Checks a user for an override in a given project and service (defaults to Github). Checks for expirations and exclusions as well.
   *
   * @param {*} username the EF username of the user to lookup for overrides
   * @param {*} project the project ID to use when matching overrides
   * @param {*} serviceType optional, the service type of the caller, either GITHUB or GITLAB
   * @returns blocked if the user has been excluded, the permission string if overridden for a service, or undefined if no changes.
   */
  checkUserForOverride(username = '', project = '', serviceType = ServiceTypes.GITHUB) {
    // check if the excludes contains the user
    if (this.#data.excluded.includes(username)) {
      this.#logger.debug(`Detected excluded/blocked user for ${username}, returning blocked state value`);
      return BLOCKED_PERMISSIONS_OUT;
    }

    // check if the user has an entry for the current project
    const userOverrides = this.#data.overrides.filter(userEntry => userEntry.name === username
      && userEntry.target_projects.includes(project));
    // if there are no overrides for user given the project, return no override values
    if (userOverrides.length === 0) {
      if (this.#verbose) {
        this.#logger.debug(`User ${username} has no overrides for current project`);
      }
      return;
    }

    // check that the known overrides aren't expired
    const dateCheckedOverrides = userOverrides.filter(override => override.expiry > new Date());
    if (dateCheckedOverrides.length === 0) {
      if (this.#verbose) {
        this.#logger.debug(`User ${username} had overrides that are now expired, returning no overrides`);
      }
      return;
    } else if (dateCheckedOverrides.length > 1) {
      this.#logger.warn(`Multiple valid overrides exist for user ${username}, looking for highest granted permission`);
      // do the lookup that iterates over multiple entries
      return getHighestPermissionLevel(dateCheckedOverrides, serviceType);
    }
    this.#logger.info(`User ${username} has override of ${dateCheckedOverrides[0].permission} for project ${project}`);
    return PermissionsEnum[dateCheckedOverrides[0].permission](serviceType);
  }

  /**
   * Using the base overrides data, searches for non-expired overrides for the given project, converting the filtered list to include the
   * relevant permission for the given service.
   *
   * @param {string} projectId the EF project ID targeted, e.g. 'technology.dash'
   * @param {*} serviceType the service to target with the permissions conversion, e.g. 'GITHUB' or 'GITLAB'.
   * @returns list of users with overridden permissions for the given project.
   */
  getUsersForProject(projectId = '', serviceType = ServiceTypes.GITHUB) {
    return this.#data.overrides.filter(o => o.target_projects.includes(projectId) && o.expiry > new Date()).map(o => {
      return {
        expiry: o.expiry,
        name: o.name,
        target_projects: o.target_projects,
        permission: PermissionsEnum[o.permission](serviceType)
      };
    });
  }
}

/**
 * Retrieves the highest permission when multiple overrides are set for a single user and project combo.
 *
 * @param {*} overrides the conflicting overrides for a single user in a project
 * @param {*} serviceType the service affected by the results
 * @returns the permission value for the highest available permission, converted for the relevant service.
 */
function getHighestPermissionLevel(overrides = [], serviceType = ServiceTypes.GITHUB) {
  let highestPermission = PermissionsEnum.READ;
  for (let idx in overrides) {
    const currentOverride = overrides[idx];
    let convertedPermission = PermissionsEnum[currentOverride.permission];
    // check if the permission set in the override is valid, and if it's higher than the current permission
    // uses the gitlab permission fetch as it returns a number, which is easy to compare
    if (convertedPermission !== undefined && typeof convertedPermission === 'function'
      && (highestPermission(ServiceTypes.GITLAB) < convertedPermission(ServiceTypes.GITLAB) || currentOverride.permission === 'ADMIN')) {
      // set the method for calculating
      highestPermission = convertedPermission;
    }
  }
  // use the method at the end to get the actual value for permissions
  return highestPermission(serviceType);
}

module.exports.UserPermissionsOverride = UserPermissionsOverride;
module.exports.BLOCKED_PERMISSIONS_OUT = BLOCKED_PERMISSIONS_OUT;
