/** **************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
var chai = require('chai');
var expect = chai.expect;

const { UserPermissionsOverride } = require('../src/teams/UserPermissionsOverride.js');
const { castJsonToOverrideFormat } = require('eclipsefdn-api-support');
const { ServiceTypes } = require('../src/teams/StaticTeamManager.js');
const { ZodError } = require('zod');
// pull in sample JSON files for some of the more manual tests
const sampleJson = JSON.stringify(require('./perms/test.json'));
const badJson = JSON.stringify(require('./perms/invalid_format.json'));
// needs to be cast for easy usage
const sample = castJsonToOverrideFormat(sampleJson);
describe('UserPermissionsOverride', function() {
  let defaultUpo;
  beforeEach(function() {
    defaultUpo = new UserPermissionsOverride(`${__dirname}/perms/test.json`);
  });

  describe('#constructor', function() {
    describe('success', function() {
        it('should allow empty excluded property', function () {
            // this case has a missing field, so it would be malformed
            expect(() => new UserPermissionsOverride(`${__dirname}/perms/empty_excludes.json`)).to.not.throw;
        });
        it('should allow empty overrides property', function () {
            // this case has a missing field, so it would be malformed
            expect(() => new UserPermissionsOverride(`${__dirname}/perms/empty_overrides.json`)).to.not.throw;
        });
    });
    describe('failure', function() {
        it('should fail when location isn\'t valid JSON', function () {
            // this case has a missing field, so it would be malformed
            expect(() => new UserPermissionsOverride(`${__dirname}/perms/invalid_format.json`)).to.throw();
        });
        it('should fail when location doesn\'t point to a valid file', function () {
            // this case has a missing file, so it should fail to load
            expect(() => new UserPermissionsOverride(`${__dirname}/perms/does_not_exist.json`)).to.throw();
        });
        it('should fail when override permissions are invalid', function () {
            // this case has a missing field, so it would be malformed
            expect(() => new UserPermissionsOverride(`${__dirname}/perms/bad_perms.json`)).to.throw();
        });
    });
  });

  describe('checkUserForOverride', function() {
    describe('success', function() {
      it('should return a value when there is a valid user and project match', function() {
        expect(defaultUpo.checkUserForOverride('malowe', 'technology.dash')).to.equal('maintain');
      });
      it('should return a relevant value when a service type is passed', function() {
        expect(defaultUpo.checkUserForOverride('malowe', 'technology.dash', ServiceTypes.GITLAB)).to.equal('40');
      });
      it('should return undefined for users that aren\'t present', function() {
        expect(defaultUpo.checkUserForOverride('test_user', 'sample')).to.be.undefined;
      });
      it('should return undefined for present users when a non-matching project is passed', function() {
        expect(defaultUpo.checkUserForOverride('malowe', 'sample')).to.be.undefined;
      });
      it('should return undefined for matching entries that are expired', function() {
        expect(defaultUpo.checkUserForOverride('webdev', 'technology.dash')).to.be.undefined;
      });
      it('should return "blocked" for users in the excluded section', function() {
        expect(defaultUpo.checkUserForOverride('webdev_2', 'technology.dash')).to.equal('blocked');
      });
      it('should return "blocked" for users in the override and excluded section', function() {
        expect(defaultUpo.checkUserForOverride('webdev_3', 'technology.dash')).to.equal('blocked');
      });
    });
  });

  describe('getUsersForProject', function() {
    describe('success', function() {
      it('should return a list of users for projects', function () {
        expect(defaultUpo.getUsersForProject('cool.project')).to.have.lengthOf(1);
      });
      it('should filter out expired entries', function () {
        const targetProject = 'cool.project';
        // chai doesn't handle list assertions well, so do some manual processing
        const out = defaultUpo.getUsersForProject(targetProject);

        // filter out entries that aren't expired, and check that there are none
        expect(out.filter(o => o.expiry < new Date())).to.have.lengthOf(0);
        // compare raw to assert the expired entry exists in base
        expect(sample.overrides.filter(o => o.target_projects.includes(targetProject) && o.expiry > new Date())).to.have.lengthOf.above(0);
      });
      it('should convert permissions to the given service type', function () {
        // check both supported service types with an example
        expect(defaultUpo.getUsersForProject('cool.project')).to.have.nested.property('[0].permission', 'maintain');
        expect(defaultUpo.getUsersForProject('cool.project', 'GITLAB')).to.have.nested.property('[0].permission', '40');
      });
      it('should return an empty list for projects w/ no entries', function () {
        expect(defaultUpo.getUsersForProject('some.other-project')).to.eql([]);
      });
    });
  });
});