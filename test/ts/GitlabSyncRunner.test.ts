/** ***************************************************************
 Copyright (C) 2024 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
import Chai from 'chai';
import ChaiAsPromised from 'chai-as-promised';
import { jest, describe, it, afterAll, afterEach, beforeAll, beforeEach } from '@jest/globals';
import { SpiedFunction } from 'jest-mock';
import { GitlabSyncRunner, GroupCache } from '../../src/scripts/gl/GitlabSyncRunner';
import { GitlabWrapper, NonAdminAccessLevel } from '../../src/scripts/gl/GitlabWrapper';

import { EclipseAPI } from '../../src/eclipse/EclipseAPI';
import { EclipseProject, InterestGroup, UserRef } from '../../src/interfaces/EclipseApi';
import { GroupSchema, MemberSchema, SimpleUserSchema, ProjectSchema } from '@gitbeaker/rest';
import { StaticTeamManager } from '../../src/teams/StaticTeamManager';

const expect = Chai.expect;

// Chai deferred assertions break without this: https://github.com/domenic/chai-as-promised/issues/41#issuecomment-208068569
Chai.should();
// set chai as promised into chai framework
Chai.use(ChaiAsPromised);

let groupIdx = 1;
let projectIdx = 1;
let userIdx = 1;
// Gitlab groups default values
const baseGroups: GroupSchema[] = [];
baseGroups.push(mockGroup({ name: 'base' })); // 1
baseGroups.push(mockGroup({ parentId: 1, name: 'sub-base' })); // 2
baseGroups.push(mockGroup({ parentId: 2, name: 'sub-2-1' })); // 3
baseGroups.push(mockGroup({ parentId: 3, name: 'sub-3-1' })); // 4
baseGroups.push(mockGroup({ parentId: 2, name: 'sub-2-2' })); // 5
baseGroups.push(mockGroup({ parentId: 2, name: 'sub-2-3' })); // 6
baseGroups.push(mockGroup({ name: 'unrelated-group' }));  // 7
baseGroups.push(mockGroup({ parentId: 7, name: 'sub-unrelated' })); // 8
baseGroups.push(mockGroup({ parentId: 1, name: 'sample' })); // 9
baseGroups.push(mockGroup({ name: 'security' })); // 10
baseGroups.push(mockGroup({ parentId: 2, name: 'interest-1' })); // 11
baseGroups.push(mockGroup({ parentId: 2, name: 'interest-2' })); // 12

// gitlab projects for testing
const baseProjects: ProjectSchema[] = [];
baseProjects.push(mockProject({ name: 'base', parentId: 10 }));
baseProjects.push(mockProject({ parentId: 10 }));

// used in Eclipse API user fetches for more consistent mocking
const sampleUserStore = generateSampleUserStore();
// IG store for easier mocking
const sampleIGStore = [
  generateSampleIG({
    id: 1, projectId: 'test.interest-1', leads: [
      { username: 'malowe', url: 'https://api.eclipse.org/account/profile/malowe' }
    ], participants: [
      { username: 'epoirier', url: 'https://api.eclipse.org/account/profile/epoirier' }
    ], projectGroup: 'base/sub-base/interest-1'
  }),
  generateSampleIG({
    id: 2, projectId: 'test.interest-2', leads: [
      { username: 'epoirier', url: 'https://api.eclipse.org/account/profile/epoirier' }
    ], participants: [
      { username: 'cguindon', url: 'https://api.eclipse.org/account/profile/cguindon' }
    ], projectGroup: 'base/sub-base/interest-2'
  }),
];
const sampleGlUserStore = {
  'test-user-1': mockUser('test-user-1'),
  'test-user-2': mockUser('test-user-2'),
  'malowe': mockUser('malowe'),
  'epoirier': mockUser('epoirier'),
  'cguindon': mockUser('cguindon'),
  'webdev_2': mockUser('webdev_2'),
  'doofenshmirtz': mockUser('doofenshmirtz'),
  'sample-bot': mockUser('sample-bot', true),
  'Webdev_3': mockUser('Webdev_3'),
};

describe('GitlabSyncRunner', function () {
  describe('#getGroup', function () {
    describe('success', function () {
      const runner = getGitlabRunner();
      let groupSpy;
      beforeAll(() => {
        // mock the calls needed for root group calling
        mockRootGroupCalls();
        mockEclipseAPI();
        return runner.prepareCaches();
      }, 20000);
      beforeEach(() => {
        groupSpy = mockRootGroupCalls();
      })
      afterEach(() => {
        jest.restoreAllMocks();
      });
      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should be able to fetch the root group', () => {
        expect(runner.getGroup('base')).to.eventually.deep.property('_self', baseGroups[0]);
      });
      it('should be able to fetch nested groups', () => {
        expect(runner.getGroup('base/sub-base/sub-2-1')).to.eventually.deep.property('_self', baseGroups[2]);
      });
      it('should create missing groups under root', done => {
        Chai.expect(runner.getGroup('base/non-existant-base')).fulfilled.then(value => {
          // getting the value again should return the same value
          expect(runner.getGroup('base/non-existant-base')).to.eventually.deep.equal(value);
          // create group call should only be called once
          expect(groupSpy.mock.calls.length).to.equal(1);
        }).should.notify(done);
      });
      it('should return null for groups outside of the configured root group', () => {
        expect(runner.getGroup('unrelated-group')).to.eventually.be.null;
        expect(runner.getGroup('unrelated-group/sub-unrelated')).to.eventually.be.null;
      });
    });
  });
  describe('#shouldRemoveUser', function () {
    describe('success', function () {
      const runner = getGitlabRunner();
      let groupSpy;
      beforeAll(() => {
        // mock the calls needed for root group calling
        mockRootGroupCalls();
        mockEclipseAPI();
        return runner.prepareCaches();
      }, 20000);
      beforeEach(() => {
        groupSpy = mockRootGroupCalls();
      })
      afterEach(() => {
        jest.restoreAllMocks();
      });
      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should keep users that are present in list', () => {
        expect(runner.shouldRemoveUser(mockMember('malowe', 20),
          [{ accessLevel: 20, projectNamespace: '', url: '', username: 'malowe' }], [])).equal(false);
      });
      it('should match users regardless of casing of username', () => {
        expect(runner.shouldRemoveUser(mockMember('Webdev_3', 20),
          [{ accessLevel: 20, projectNamespace: '', url: '', username: 'webdev_3' }], [])).equal(false);
      });
    });
  });
  describe('#performStaticTeamSync', function () {
    describe('success', function () {
      const runner = getGitlabRunner();
      const baseStaticTeam = {
        "repos": [
          "https://gitlab.eclipse.org/base"
        ],
        "teamName": 'sample-team',
        "members": [
          {
            "name": "malowe",
            "url": "https://api.eclipse.org/account/profile/malowe",
            "expiration": ""
          }
        ],
        "permission": "TRIAGE",
        "expiration": "2099-12-31"
      };
      const baseStm = runner.stm;
      beforeAll(() => {
        // mock the calls needed for root group calling
        mockRootGroupCalls();
        mockEclipseAPI();
        return runner.prepareCaches();
      }, 20000);
      beforeEach(() => {
        mockProjectCalls();
      })
      afterEach(() => {
        jest.restoreAllMocks();
        // revert the stm to the base
        runner.stm = baseStm;
      });
      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should be able to sync to root group', async () => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': []
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        runner.stm = new StaticTeamManager([baseStaticTeam]);
        await runner.performStaticTeamSync();
        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(1);
        // edit group members should never be called when there are no updates
        expect(editSpy.mock.calls.length).to.equal(0);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({ '1': { '3': 20 } });
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });
      it('should not sync expired teams', async () => {
        const teamName = 'sample-team1';
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': []
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        const modifiedTeam = Object.assign({}, baseStaticTeam);
        modifiedTeam.expiration = '01-01-2020';
        modifiedTeam.teamName = teamName;
        runner.stm = new StaticTeamManager([modifiedTeam]);
        await runner.performStaticTeamSync();
        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates
        expect(editSpy.mock.calls.length).to.equal(0);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});

        // check that the static team doesn't get tracked for later
        const group = await runner.getGroup('base');
        expect(group).to.not.be.null;
        expect(group!.projectTargets).to.not.contain(teamName);
      });

      it('should track static team to prevent deletion', async () => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': []
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        runner.stm = new StaticTeamManager([baseStaticTeam]);
        await runner.performStaticTeamSync();

        // check that the static team gets properly tracked for later
        const group = await runner.getGroup('base');
        expect(group).to.not.be.null;
        expect(group!.projectTargets).to.contain('sample-team');

        // check that the project is in the proper state
        const psudoProject = runner.eclipseProjectCache['sample-team'];
        expect(psudoProject).to.not.be.undefined;
        expect(psudoProject.contributors).to.not.be.empty;
        expect(psudoProject.committers).to.be.empty;
        expect(psudoProject.project_leads).to.be.empty;
      });
      it('should allow for write access permissions', async () => {
        const teamName = 'sample-team2';
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': []
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        const modifiedTeam = Object.assign({}, baseStaticTeam);
        modifiedTeam.permission = 'WRITE';
        modifiedTeam.teamName = teamName;
        runner.stm = new StaticTeamManager([modifiedTeam]);
        await runner.performStaticTeamSync();

        // check that the project is in the proper state
        const psudoProject = runner.eclipseProjectCache[teamName];
        expect(psudoProject).to.not.be.undefined;
        expect(psudoProject.contributors).to.be.empty;
        expect(psudoProject.committers).to.not.be.empty;
        expect(psudoProject.project_leads).to.be.empty;
      });
      it('should allow for maintain access permissions', async () => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': []
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        const modifiedTeam = Object.assign({}, baseStaticTeam);
        const teamName = 'sample-team3';
        modifiedTeam.permission = 'MAINTAIN';
        modifiedTeam.teamName = teamName;
        runner.stm = new StaticTeamManager([modifiedTeam]);
        await runner.performStaticTeamSync();

        // check that the project is in the proper state
        const psudoProject = runner.eclipseProjectCache[teamName];
        expect(psudoProject).to.not.be.undefined;
        expect(psudoProject.contributors).to.be.empty;
        expect(psudoProject.committers).to.be.empty;
        expect(psudoProject.project_leads).to.not.be.empty;
      });

      it('should ignore properly set users', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': [mockMember('malowe', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        runner.stm = new StaticTeamManager([baseStaticTeam]);
        Chai.expect(runner.performStaticTeamSync()).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // add project members should never be called when there are no updates
          expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          expect(updateProjectMemberCalls).to.deep.equal({});
        }).should.notify(done);
      });

      it('should fix users with bad permissions', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': [mockMember('malowe', 30)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        runner.stm = new StaticTeamManager([baseStaticTeam]);
        Chai.expect(runner.performStaticTeamSync()).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(1);
          // add project members should never be called when there are no updates
          expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({ '1': { '3': 20 } });
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          expect(updateProjectMemberCalls).to.deep.equal({});
        }).should.notify(done);
      });
    });
  });
  describe('#performIGSync', function () {
    describe('success', function () {
      const runner = getGitlabRunner();
      let groupSpy;
      beforeAll(() => {
        // mock the calls needed for root group calling
        mockRootGroupCalls();
      }, 20000);
      beforeEach(() => {
        groupSpy = mockRootGroupCalls();
      })
      afterEach(() => {
        jest.restoreAllMocks();
      });
      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should not attempt updates for up-to-date interest groups', async () => {
        // done separately so we can control which IGs get sync'd
        mockEclipseAPI();
        await runner.prepareCaches();

        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '11': [mockMember('malowe', 40), mockMember('epoirier', 30)],
          '12': [mockMember('cguindon', 30), mockMember('epoirier', 40)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        await runner.performIGSync('base/sub-base');
        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates
        expect(editSpy.mock.calls.length).to.equal(0);
        // rm group members should never be called when there are no updates
        expect(rmSpy.mock.calls.length).to.equal(0);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });

      it('should not add users from terminated IGs', async () => {
        const localIGs = structuredClone(sampleIGStore);
        // first IG should be linked to interest-1, which is group 11
        localIGs[0].state = 'terminated';
        // done separately so we can control which IGs get sync'd
        mockEclipseAPI(localIGs);
        await runner.prepareCaches();

        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '11': [],
          '12': [mockMember('cguindon', 30), mockMember('epoirier', 40)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        await runner.performIGSync('base/sub-base');
        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates
        expect(editSpy.mock.calls.length).to.equal(0);
        // rm group members should never be called when there are no updates
        expect(rmSpy.mock.calls.length).to.equal(0);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        expect(rmCalls).to.deep.equal({});
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });
    });
  });

  describe('#cleanupGroups', function () {
    describe('success', function () {
      const runner = getGitlabRunner();
      let groupSpy;
      beforeAll(() => {
        // mock the calls needed for root group calling
        mockRootGroupCalls();
      });
      beforeEach(() => {
        groupSpy = mockRootGroupCalls();
      })
      afterEach(() => {
        jest.restoreAllMocks();
      });
      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should remove unneeded users from projects when necessary', async () => {
        mockEclipseAPI();
        await runner.prepareCaches();
        // doofenshmirtz is not expected and would be removed in this case
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [
            mockMember('malowe', 40),
            mockMember('cguindon', 40),
            mockMember('epoirier', 30),
            mockMember('webdev_2', 20),
            mockMember('doofenshmirtz', 20)
          ]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        await runner.performEclipseProjectSync({ 'sample': generateSampleProjects()[0] }, 'base');
        await runner.cleanupGroups();
        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates
        expect(editSpy.mock.calls.length).to.equal(0);
        // rm group members should never be called when there are no updates
        expect(rmSpy.mock.calls.length).to.equal(1);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        expect(rmCalls).to.deep.equal({ '9': [7] });
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });
      it('should not attempt updates for up-to-date projects', async () => {
        mockEclipseAPI();
        await runner.prepareCaches();
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        await runner.performEclipseProjectSync({ 'sample': generateSampleProjects()[0] }, 'base');
        await runner.cleanupGroups();
        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates
        expect(editSpy.mock.calls.length).to.equal(0);
        // rm group members should never be called when there are no updates
        expect(rmSpy.mock.calls.length).to.equal(0);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        expect(rmCalls).to.deep.equal({});
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });

      it('should perform no updates for up-to-date Interest Groups', async () => {
        // done separately so we can control which IGs get sync'd
        mockEclipseAPI(sampleIGStore);
        await runner.prepareCaches();

        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '11': [mockMember('malowe', 40), mockMember('epoirier', 30)],
          '12': [mockMember('cguindon', 30), mockMember('epoirier', 40)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        await runner.performIGSync('base/sub-base');
        await runner.cleanupGroups();

        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates, only removals
        expect(editSpy.mock.calls.length).to.equal(0);
        // rm group members should be 2 as 2 users would be removed from the terminated group
        expect(rmSpy.mock.calls.length).to.equal(0);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        expect(rmCalls).to.deep.equal({});
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });
      it('should remove users from terminated IGs', async () => {
        const localIGs = structuredClone(sampleIGStore);
        // first IG should be linked to interest-1, which is group 11
        localIGs[0].state = 'terminated';
        // done separately so we can control which IGs get sync'd
        mockEclipseAPI(localIGs);
        await runner.prepareCaches();

        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '11': [mockMember('malowe', 40), mockMember('epoirier', 30)],
          '12': [mockMember('cguindon', 30), mockMember('epoirier', 40)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        const b = await runner.performIGSync('base/sub-base');
        const a = await runner.cleanupGroups();

        // add group members should never be called when there are no updates
        expect(addSpy.mock.calls.length).to.equal(0);
        // edit group members should never be called when there are no updates, only removals
        expect(editSpy.mock.calls.length).to.equal(0);
        // rm group members should be 2 as 2 users would be removed from the terminated group
        expect(rmSpy.mock.calls.length).to.equal(2);
        // add project members should never be called when there are no updates
        expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
        // edit project members should never be called when there are no updates
        expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
        // update tracking records should also be empty
        expect(editCalls).to.deep.equal({});
        expect(updateCalls).to.deep.equal({});
        // uses GL user id rather than names, 3+4 correspond to malowe + epoirier
        expect(rmCalls).to.deep.equal({ '11': [3, 4] });
        expect(editProjectMemberCalls).to.deep.equal({});
        expect(updateProjectMemberCalls).to.deep.equal({});
      });
    });
  });

  describe('#performEclipseProjectSync', function () {
    describe('success', function () {
      const runner = getGitlabRunner();
      beforeAll(() => {
        // mock the calls needed for root group calling
        mockRootGroupCalls();
        mockEclipseAPI();
        return runner.prepareCaches();
      }, 20000);
      beforeEach(() => {
        mockProjectCalls();
      })
      afterEach(() => {
        jest.restoreAllMocks();
      });
      afterAll(() => {
        jest.resetAllMocks();
      });

      it('should not attempt updates for up-to-date projects', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        Chai.expect(runner.performEclipseProjectSync({ 'sample': generateSampleProjects()[0] }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should never be called when there are no updates
          expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          expect(updateProjectMemberCalls).to.deep.equal({});
        }).should.notify(done);
      });
      it('should add missing group members from project', done => {
        // epoirier removed from group roster for call
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('webdev_2', 20)]
        });
        const [editProjectSpy, addToProjectSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        Chai.expect(runner.performEclipseProjectSync({ 'sample': generateSampleProjects()[0] }, 'base')).fulfilled.then(value => {
          expect(editCalls).to.deep.equal({});
          // epoirier ID expected to be 4, update should be to set DEVELOPER (30)
          expect(updateCalls).to.deep.equal({
            "9": {
              "4": 30,
            },
          });
          // add group members should be called once for one missing user
          expect(addSpy.mock.calls.length).to.equal(1);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
        }).should.notify(done);
      });
      it('should update group members with bad perms from project', done => {
        // cguindon wrong perms from group roster for call (should be 40)
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 30), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectSpy, addToProjectSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        Chai.expect(runner.performEclipseProjectSync({ 'sample': generateSampleProjects()[0] }, 'base')).fulfilled.then(value => {
          // cguindon ID expected to be 5, update should be to set MAINTAINER (40)
          expect(editCalls).to.deep.equal({
            "9": {
              "5": 40,
            },
          });
          expect(updateCalls).to.deep.equal({});
          // add group members should not be called when fixing user permissions
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should be called once for the single user update
          expect(editSpy.mock.calls.length).to.equal(1);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
        }).should.notify(done);
      });
      // iss helpdesk#5073 regression test
      it('should allow for nested groups around TLPs', done => {
        // cguindon wrong perms from group roster for call (should be 40)
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '1': [mockMember('epoirier', 40), mockMember('malowe', 30)],
          '9': [mockMember('malowe', 40), mockMember('cguindon', 30), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectSpy, addToProjectSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        Chai.expect(runner.performEclipseProjectSync({
          'sample': generateSampleProjects()[0],
          'base': generateProject('base', 'base', 'base', 'base', [], [{
            username: 'malowe',
            url: 'https://api.eclipse.org/account/profile/malowe',
          }], [{
            username: 'epoirier',
            url: 'https://api.eclipse.org/account/profile/epoirier',
          }])
        }, 'base')).fulfilled.then(value => {
          // cguindon ID expected to be 5, update should be to set MAINTAINER (40)
          expect(editCalls).to.deep.equal({
            "9": {
              "5": 40,
            },
          });
          expect(updateCalls).to.deep.equal({});
          // add group members should not be called when fixing user permissions
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should be called once for the single user update
          expect(editSpy.mock.calls.length).to.equal(1);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
        }).should.notify(done);
      });
      it('should add missing bot users on groups', done => {
        // fakes that the sample project has a bot w/ the name 'sample-bot'
        runner.botsCache = {
          'sample': ['sample-bot']
        };

        // sample-bot should be retained at maintainer level permissions
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectSpy, addToProjectSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({});
        Chai.expect(runner.performEclipseProjectSync({ 'sample': generateSampleProjects()[0] }, 'base')).fulfilled.then(value => {
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({
            "9": {
              "8": 50,
            },
          });
          // add group members should be called once to add the missing bot
          expect(addSpy.mock.calls.length).to.equal(1);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          runner.botsCache = {};
        }).should.notify(done);
      });
      /* Security project update tests */
      it('should allow for including committers in security team', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({
        });
        const project = generateSampleProjects()[0];
        project.security_team.groups.include_committers = true;
        Chai.expect(runner.performEclipseProjectSync({ 'sample': project }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should never be called when there are no updates
          expect(addProjectMemberSpy.mock.calls.length).to.equal(2);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          // 3 and 4 are Martin and Eric
          expect(updateProjectMemberCalls).to.deep.equal({
            "2": {
              "3": 20,
              "4": 20,
            }
          });
        }).should.notify(done);
      });
      it('should allow for including project leads in security team', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({
        });
        const project = generateSampleProjects()[0];
        project.security_team.groups.include_project_leads = true;
        Chai.expect(runner.performEclipseProjectSync({ 'sample': project }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should be called twice, once for each committer on project
          expect(addProjectMemberSpy.mock.calls.length).to.equal(2);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          // 3 and 5 are Martin and Chris
          expect(updateProjectMemberCalls).to.deep.equal({
            "2": {
              "3": 20,
              "5": 20,
            }
          });
        }).should.notify(done);
      });
      it('should deduplicate repeated users in security team update', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({
        });
        const project = generateSampleProjects()[0];
        project.security_team.groups.include_project_leads = true;
        project.security_team.individual_members.push({ url: 'https://api.eclipse.org/account/profile/malowe', username: 'malowe', });
        Chai.expect(runner.performEclipseProjectSync({ 'sample': project }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should be called twice, even w/ 1 in members and 2 in PLs, due to dedupe
          expect(addProjectMemberSpy.mock.calls.length).to.equal(2);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          // 3 and 5 are Martin and Chris
          expect(updateProjectMemberCalls).to.deep.equal({
            "2": {
              "3": 20,
              "5": 20,
            }
          });
        }).should.notify(done);
      });
      it('should allow for PLs and committers to be included at same time', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({
        });
        const project = generateSampleProjects()[0];
        project.security_team.groups.include_project_leads = true;
        project.security_team.groups.include_committers = true;
        Chai.expect(runner.performEclipseProjectSync({ 'sample': project }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should be called 3 times, once for each unique PL+committer 
          expect(addProjectMemberSpy.mock.calls.length).to.equal(3);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          // 3, 4, and 5 are Martin, Eric, and Chris
          expect(updateProjectMemberCalls).to.deep.equal({
            "2": {
              "3": 20,
              "4": 20,
              "5": 20,
            }
          });
        }).should.notify(done);
      });
      it('should not update already up-to-date security team', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({
          '2': [mockMember('malowe', 20), mockMember('cguindon', 20)]
        });
        const project = generateSampleProjects()[0];
        project.security_team.groups.include_project_leads = true;
        Chai.expect(runner.performEclipseProjectSync({ 'sample': project }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should be called twice, once for each committer on project
          expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(0);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({});
          expect(updateProjectMemberCalls).to.deep.equal({});
        }).should.notify(done);
      });
      it('should update users who have bad perms in security project', done => {
        // should accurately reflect expected up-to-date state of users in group
        const [editSpy, addSpy, rmSpy, editCalls, updateCalls, rmCalls] = mockGitlabGroupUpdateCalls({
          '9': [mockMember('malowe', 40), mockMember('cguindon', 40), mockMember('epoirier', 30), mockMember('webdev_2', 20)]
        });
        // in this case, epoirier would be removed
        const [editProjectMemberSpy, addProjectMemberSpy, editProjectMemberCalls, updateProjectMemberCalls] = mockGitlabProjectUpdateCalls({
          '2': [mockMember('malowe', 30), mockMember('cguindon', 20)]
        });
        const project = generateSampleProjects()[0];
        project.security_team.groups.include_project_leads = true;
        Chai.expect(runner.performEclipseProjectSync({ 'sample': project }, 'base')).fulfilled.then(value => {
          // add group members should never be called when there are no updates
          expect(addSpy.mock.calls.length).to.equal(0);
          // edit group members should never be called when there are no updates
          expect(editSpy.mock.calls.length).to.equal(0);
          // rm group members should never be called when there are no updates
          expect(rmSpy.mock.calls.length).to.equal(0);
          // add project members should be called twice, once for each committer on project
          expect(addProjectMemberSpy.mock.calls.length).to.equal(0);
          // edit project members should never be called when there are no updates
          expect(editProjectMemberSpy.mock.calls.length).to.equal(1);
          // update tracking records should also be empty
          expect(editCalls).to.deep.equal({});
          expect(updateCalls).to.deep.equal({});
          expect(editProjectMemberCalls).to.deep.equal({
            '2': {
              '3': 20
            }
          });
          expect(updateProjectMemberCalls).to.deep.equal({
          });
        }).should.notify(done);
      });
    });
  });
});

/**
 * Used to intercept remote call to Gitlab to return test data for groups
 */
function mockRootGroupCalls() {
  jest.spyOn(GitlabWrapper.prototype, 'getAllGroups').mockImplementationOnce(() => {
    return baseGroups;
  });
  return jest.spyOn(GitlabWrapper.prototype, 'createGroup').mockImplementation((name, path, opts) => {
    return Promise.resolve(mockGroup({ name: name }));
  });
}

function mockProjectCalls() {
  jest.spyOn(GitlabWrapper.prototype, 'getAllProjects').mockImplementationOnce(() => {
    return baseProjects;
  });
}

/**
 * Mocks the group member add, edit, and retrieval, as well as the user fetch methods in the Gitlab wrapper. Returns spies and update
 * records for the add user to group and edit group user functions for test validation.
 *
 * @param groupMemberStore a map containing lists of MemberSchemas mapped to the ID of the group they would belong to.
 * @returns edit and add group user spies, then the edit and add update tracking maps.
 */
function mockGitlabGroupUpdateCalls(groupMemberStore: Record<number, MemberSchema[]>): [
  SpiedFunction<(groupId: string | number, userId: number, accessLevel: NonAdminAccessLevel) => Promise<MemberSchema | null>>,
  SpiedFunction<(groupId: string | number, userId: number, accessLevel: NonAdminAccessLevel) => Promise<MemberSchema | null>>,
  SpiedFunction<(groupId: string | number, userId: number) => Promise<void>>,
  Record<string, any>,
  Record<string, any>,
  Record<string, string[]>] {
  jest.spyOn(GitlabWrapper.prototype, 'getNamedUser').mockImplementation((userName) => {
    return sampleGlUserStore[userName];
  });
  jest.spyOn(GitlabWrapper.prototype, 'getGroupMembers').mockImplementation(async (groupId) => {
    return groupMemberStore[groupId] || null;
  });
  const groupMemberEdits = {};
  const editSpy = jest.spyOn(GitlabWrapper.prototype, 'editGroupMember').mockImplementation((groupId, userId, perms) => {
    if (groupMemberEdits[groupId] === undefined) {
      groupMemberEdits[groupId] = {};
    }
    groupMemberEdits[groupId][userId] = perms;
    return groupMemberStore[groupId][userId];
  });
  const groupMemberUpdates = {};
  const addSpy = jest.spyOn(GitlabWrapper.prototype, 'addGroupMember').mockImplementation((groupId, userId, perms) => {
    if (groupMemberUpdates[groupId] === undefined) {
      groupMemberUpdates[groupId] = {};
    }
    groupMemberUpdates[groupId][userId] = perms;
    return groupMemberStore[groupId][userId];
  });
  const groupMemberRms = {};
  const removeSpy = jest.spyOn(GitlabWrapper.prototype, 'removeGroupMember').mockImplementation((id, userId) => {
    if (groupMemberRms[id] === undefined) {
      groupMemberRms[id] = [];
    }
    groupMemberRms[id].push(userId);
    return Promise.resolve();
  });
  return [editSpy, addSpy, removeSpy, groupMemberEdits, groupMemberUpdates, groupMemberRms];
}

function mockGitlabProjectUpdateCalls(projectMemberStore: Record<number, MemberSchema[]>): [
  SpiedFunction<(projectId: string | number, userId: number, accessLevel: NonAdminAccessLevel) => Promise<MemberSchema | null>>,
  SpiedFunction<(projectId: string | number, userId: number, accessLevel: NonAdminAccessLevel) => Promise<MemberSchema | null>>,
  Record<string, any>,
  Record<string, any>] {
  jest.spyOn(GitlabWrapper.prototype, 'getAllProjectMembers').mockImplementation(async (projectId) => {
    return projectMemberStore[projectId] || [];
  });
  const projectMemberEdits = {};
  const editSpy = jest.spyOn(GitlabWrapper.prototype, 'editProjectMember').mockImplementation(async (projectId, userId, perms) => {
    if (projectMemberEdits[projectId] === undefined) {
      projectMemberEdits[projectId] = {};
    }
    projectMemberEdits[projectId][userId] = perms;
    // update the store and return
    var user: MemberSchema = (projectMemberStore[projectId] as MemberSchema[]).filter(u => u.id === userId)[0];
    user.access_level = perms;
    return user;
  });
  const projectMemberUpdates = {};
  const addSpy = jest.spyOn(GitlabWrapper.prototype, 'addProjectMember').mockImplementation(async (projectId, userId, perms) => {
    if (projectMemberUpdates[projectId] === undefined) {
      projectMemberUpdates[projectId] = {};
    }
    projectMemberUpdates[projectId][userId] = perms;
    var user = Object.values(sampleGlUserStore).filter(u => u.id === userId)[0];
    if (projectMemberStore[projectId] === undefined) {
      projectMemberStore[projectId] = [];
    }
    // create a base schema to stub
    const member: MemberSchema = mockMember(user.username, perms);
    projectMemberStore[projectId].push(member);
    return member;
  });
  return [editSpy, addSpy, projectMemberEdits, projectMemberUpdates];
}

function mockEclipseAPI(interestGroups: InterestGroup[] = sampleIGStore) {
  jest.spyOn(EclipseAPI.prototype, 'eclipseAPI').mockImplementation(() => {
    return Promise.resolve(generateSampleProjects());
  });
  jest.spyOn(EclipseAPI.prototype, 'interestGroups').mockImplementation(() => {
    return Promise.resolve(structuredClone(interestGroups));
  });
  jest.spyOn(EclipseAPI.prototype, 'eclipseUser').mockImplementation((username: string) => {
    if (sampleUserStore[username] !== undefined) {
      return Promise.resolve(sampleUserStore[username]);
    }
    return Promise.resolve(null);
  });
}

function mockGroup({ parentId = -1, name = 'sample' }): GroupSchema {
  return {
    id: groupIdx++,
    name: name,
    path: name,
    full_name: parentId === -1 ? name : `${recurseGroupValue(g => g.name, parentId)}/${name}`,
    full_path: parentId === -1 ? name : `${recurseGroupValue(g => g.name, parentId)}/${name}`,
    parent_id: parentId,
    visibility: 'public',
    avatar_url: '',
    web_url: '',
    description: '',
    share_with_group_lock: false,
    require_two_factor_authentication: false,
    two_factor_grace_period: 0,
    project_creation_level: 'developer',
    auto_devops_enabled: false,
    subgroup_creation_level: 'maintainer',
    emails_disabled: true,
    mentions_disabled: true,
    lfs_enabled: false,
    default_branch_protection: 1,
    request_access_enabled: false,
    file_template_project_id: 0,
    created_at: ''
  };
}

function mockProject({ parentId = 0, name = 'sample' }): ProjectSchema {
  return {
    id: projectIdx++,
    name: name,
    avatar_url: '',
    created_at: '',
    forks_count: 0,
    http_url_to_repo: '',
    description_html: '',
    empty_repo: false,
    name_with_namespace: `${recurseGroupValue(g => g.name, parentId)}/${name}`,
    path_with_namespace: `${recurseGroupValue(g => g.name, parentId)}/${name}`,
    owner: {
      name: '',
      created_at: '',
      id: 0
    },
    visibility: 'public',
    last_activity_at: '',
    namespace: {
      avatar_url: '',
      full_path: recurseGroupValue(g => g.name, parentId),
      id: parentId,
      kind: '',
      name: '',
      path: '',
      web_url: '',
    },
    path: '',
    readme_url: '',
    ssh_url_to_repo: '',
    star_count: 0,
    web_url: '',
    default_branch: 'main',
    description: '',
    tag_list: [],
    topics: [],
    issues_enabled: false,
    open_issues_count: 0,
    merge_requests_enabled: false,
    jobs_enabled: false,
    wiki_enabled: false,
    snippets_enabled: false,
    can_create_merge_request_in: false,
    resolve_outdated_diff_discussions: false,
    container_registry_access_level: '',
    security_and_compliance_access_level: '',
    container_expiration_policy: {
      cadence: '',
      enabled: false,
      keep_n: 1,
      older_than: null,
      name_regex_delete: null,
      name_regex_keep: null,
      next_run_at: '',
    },
    updated_at: '',
    creator_id: 0,
    import_url: null,
    import_type: null,
    import_status: '',
    import_error: null,
    permissions: {
      project_access: {
        access_level: 0,
        notification_level: 0,
      },
      group_access: {
        access_level: 0,
        notification_level: 0,
      }
    },
    archived: false,
    license_url: '',
    license: {
      html_url: '',
      key: '',
      name: '',
      nickname: '',
      source_url: '',
    },
    shared_runners_enabled: false,
    group_runners_enabled: false,
    runners_token: '',
    ci_default_git_depth: 0,
    ci_forward_deployment_enabled: false,
    ci_forward_deployment_rollback_allowed: false,
    ci_allow_fork_pipelines_to_run_in_parent_project: false,
    ci_separated_caches: false,
    ci_restrict_pipeline_cancellation_role: '',
    public_jobs: false,
    shared_with_groups: null,
    repository_storage: '',
    only_allow_merge_if_pipeline_succeeds: false,
    allow_merge_on_skipped_pipeline: false,
    restrict_user_defined_variables: false,
    only_allow_merge_if_all_discussions_are_resolved: false,
    remove_source_branch_after_merge: false,
    printing_merge_requests_link_enabled: false,
    request_access_enabled: false,
    merge_method: '',
    squash_option: '',
    auto_devops_enabled: false,
    auto_devops_deploy_strategy: '',
    mirror: false,
    mirror_user_id: 0,
    mirror_trigger_builds: false,
    only_mirror_protected_branches: false,
    mirror_overwrites_diverged_branches: false,
    external_authorization_classification_label: null,
    packages_enabled: false,
    service_desk_enabled: false,
    service_desk_address: null,
    autoclose_referenced_issues: false,
    suggestion_commit_message: null,
    enforce_auth_checks_on_uploads: false,
    merge_commit_template: null,
    squash_commit_template: null,
    issue_branch_template: '',
    marked_for_deletion_on: '',
    compliance_frameworks: null,
    warn_about_potentially_unwanted_characters: false,
    container_registry_image_prefix: '',
    _links: {
      cluster_agents: '',
      events: '',
      issues: '',
      labels: '',
      members: '',
      merge_requests: '',
      repo_branches: '',
      self: '',
    }
  } as ProjectSchema;
}

function mockUser(username: string, isBot = false): SimpleUserSchema {
  return {
    id: userIdx++,
    username: username,
    name: 'Sample User',
    state: 'active',
    locked: false,
    avatar_url: 'https://gitlab.eclipse.org/uploads/-/system/user/avatar/1/avatar.png',
    web_url: `https://gitlab.eclipse.org/${username}`,
    created_at: '2020-07-09T15:11:25.117Z',
    bio: '',
    location: 'The Internet',
    public_email: '',
    skype: '',
    linkedin: '',
    twitter: '',
    discord: '',
    website_url: '',
    organization: 'Eclipse Foundation',
    job_title: '',
    pronouns: null,
    bot: isBot,
    work_information: '',
    followers: 0,
    following: 0,
    is_followed: false,
    local_time: '12:00 PM'
  };
}

function mockMember(username, accessLevel): MemberSchema {
  const u = sampleGlUserStore[username];
  if (u === undefined) throw new Error('Test improperly setup, cannot create a membership for a user that doesn\'t \'exist\'');
  return {
    ...u,
    access_level: accessLevel
  };
}

/**
 * Used to create realistic values for use in Group schema values. Recurses through existing records to generate values for nested groups, like full path or name.
 * 
 * @param extractor function that extracts the value used for constructing the recursive value
 * @param currentParentId current parent ID to retrieve for recursion.
 * @returns compiled recursed value using the extractor and the baseGroups store.
 */
function recurseGroupValue(extractor: (arg0: GroupSchema) => string, currentParentId: number): string {
  const parent = baseGroups.find(v => v.id === currentParentId);
  if (parent === undefined) throw new Error(`Bad args passed, parent with ID ${currentParentId} doesn't exist`);
  // recursively extract values through the tree
  if (parent.parent_id !== -1) {
    return `${recurseGroupValue(extractor, parent.parent_id)}/${extractor(parent)}`;
  }
  return extractor(parent);
}

function getGitlabRunner(config = {}): GitlabSyncRunner {
  return new GitlabSyncRunner({
    devMode: false,
    host: 'localhost',
    dryRun: false,
    provider: 'oauth2_generic',
    verbose: true,
    project: undefined,
    secretLocation: `${__dirname}/../secrets/gl`,
    rootGroup: 'base',
    staging: false,
    user: 'webmaster',
    ...config
  });
}

///
/// Copied test functions - we should eventually share these properly across tests
///

/**
 * Generates fake users to be used in HTTP stubbing in tests.
 */
function generateSampleUserStore() {
  return {
    'test-user-1': generateSampleUser('test-user-1'),
    'test-user-2': generateSampleUser('test-user-2'),
    'malowe': generateSampleUser('malowe'),
    'doofenshmirtz': generateSampleUser('doofenshmirtz')
  };
}
function generateSampleUser(uname, hasSignedECA = true, isCommitter = true) {
  return {
    'uid': '',
    'name': uname,
    'picture': '',
    'mail': `${uname}@eclipse-foundation.org`,
    'eca': {
      'signed': hasSignedECA,
      'can_contribute_spec_project': hasSignedECA
    },
    'is_committer': isCommitter,
    'first_name': 'Sample',
    'last_name': 'Test1',
    'full_name': 'Sample Test1',
    'publisher_agreements': {
      'open-vsx': { 'version': '1' }
    },
    'github_handle': undefined,
    'twitter_handle': '',
    'org': 'EF Sample 01',
    'org_id': 1660,
    'job_title': 'Software Developer',
    'website': '',
    'country': {
      'code': null,
      'name': null
    }, 'bio': null,
    'interests': ['Music', 'Video games', 'cats', 'Accessibility'],
    'working_groups_interests': [],
    'public_fields': [],
    'mail_history': [],
    'mail_alternate': [],
    'eca_url': `https://api.eclipse.org/account/profile/${uname}/eca`,
    'projects_url': `https://api.eclipse.org/account/profile/${uname}/projects`,
    'gerrit_url': `https://api.eclipse.org/account/profile/${uname}/gerrit`,
    'mailinglist_url': `https://api.eclipse.org/account/profile/${uname}/mailing-list`,
    'mpc_favorites_url': `https://api.eclipse.org/marketplace/favorites?name=${uname}`
  };
}
function generateSampleIG({ id, projectId, leads, participants, projectGroup, state = 'active' }): InterestGroup {
  return {
    'id': id,
    'title': projectId,
    'project_id': projectId,
    'description': {
      'summary': '',
      'full': ''
    },
    'logo': '',
    'scope': {
      'summary': '',
      'full': ''
    },
    'leads': leads,
    'participants': participants,
    'state': state,
    'gitlab': {
      'project_group': projectGroup,
      'ignored_sub_groups': []
    }
  }
}
function generateSampleProjects(): EclipseProject[] {
  return [
    generateProject('sample', 'sample', 'base/sample', 'base', [{
      username: 'webdev_2',
      url: 'https://api.eclipse.org/account/profile/webdev_2',
    }], [{
      username: 'malowe',
      url: 'https://api.eclipse.org/account/profile/malowe',
    }, {
      username: 'epoirier',
      url: 'https://api.eclipse.org/account/profile/epoirier',
    }], [{
      username: 'malowe',
      url: 'https://api.eclipse.org/account/profile/malowe',
    }, {
      username: 'cguindon',
      url: 'https://api.eclipse.org/account/profile/cguindon',
    }]),
  ];
}

function generateProject(projectId: string, shortProjectId: string, gitlabProject: string, tlp = '', contributors: UserRef[] = [],
  committers: UserRef[] = [], leads: UserRef[] = []) {
  return {
    project_id: projectId,
    short_project_id: shortProjectId,
    name: '',
    summary: '',
    gerrit_repos: [],
    logo: '',
    top_level_project: tlp,
    website_repo: [],
    website_url: '',
    tags: [],
    url: '',
    releases: [],
    github_repos: [{
      url: 'https://github.com/eclipsefdn-webdev/spider-pig',
    }],
    gitlab: {
      project_group: gitlabProject,
      ignored_sub_groups: []
    },
    contributors: contributors,
    committers: committers,
    project_leads: leads,
    working_groups: [{
      name: 'Cloud Development Tools',
      id: 'cloud-development-tools',
    }],
    spec_project_working_group: [],
    state: 'Regular',
    security_team: {
      individual_members: [],
      groups: {
        include_committers: false,
        include_project_leads: false,
      },
    }
  };
}
