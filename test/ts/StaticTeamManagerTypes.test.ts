/** ***************************************************************
 Copyright (C) 2025 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
import { expect } from 'chai';
import { describe, it } from '@jest/globals';
import { StaticTeamManager, ServiceTypes } from '../../src/teams/StaticTeamManager.js';
import { StaticTeam } from '../../src/interfaces/StaticTeamManagerTypes';

describe('StaticTeamManager', function () {
  const stm = new StaticTeamManager();
  describe('#readSecret', function () {
    describe('success', function () {
      it('should match the interface output', function () {
        // ensure that the output of the process teams default process can be successfully cast easily
        const out = stm.processTeams('GITHUB');
        const castOut = out as StaticTeam[];
        expect(castOut).to.not.be.null;
        expect(castOut.length).to.be.greaterThan(1);
      });
    });
  });
});